package u.LPHApp.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import u.LPHApp.Models.ModelAppointment;
import u.LPHApp.R;

/**
 * Adapter class for RecyclerView's Adapter.
 *
 * Created by Edward Ndukui,
 * on Wednesday, 05-Jul-17,
 * at 10:35AM.
 */
public class AdapterRVViewAppointments extends RecyclerView.Adapter<AdapterRVViewAppointments.ViewAppointmentsViewHolder> {

    private InterfaceAdapViewAppointments interAdapViewAppointments;

    private ArrayList<ModelAppointment> arylAppointments;

    public interface InterfaceAdapViewAppointments {
        void codeToDeleteSelectedAppointmentRecord(int appointmentId, int adapterPosition);
    }

    public AdapterRVViewAppointments(Context context, ArrayList<ModelAppointment> arrayListAppointments) {
        this.arylAppointments = arrayListAppointments;

        try {
            interAdapViewAppointments = (InterfaceAdapViewAppointments) context;
        } catch (ClassCastException ccex) {
            throw new ClassCastException("ViewAppointmentsActivity MUST implement InterfaceAdaptViewAppointments!");
        }
    }

    @Override
    public int getItemCount() {
        return arylAppointments.size();
    }

    @Override
    public ViewAppointmentsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return (new ViewAppointmentsViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.rowlayout_view_appointments, parent, false)));
    }

    @Override
    public void onBindViewHolder(final ViewAppointmentsViewHolder holder, int position) {

        holder.txtHospitalLocation.setText(arylAppointments.get(position).getsHospitalLocation());
        holder.txtPreferredDate.setText(arylAppointments.get(position).getsPreferredDate());
        holder.txtPreferredTime.setText(arylAppointments.get(position).getsPreferredTime());
        holder.txtSpecialist.setText(arylAppointments.get(position).getsSpecialist());
        holder.txtCondition.setText(arylAppointments.get(position).getsCondition());

        holder.imgvDelete.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Log.e("AdapterRVAppointments", "onClick() -- getAdapterPosition(): " + holder.getAdapterPosition());    //  TODO: For Testing ONLy

                int iAppointmentId = arylAppointments.get(holder.getAdapterPosition()).getiAppointmentId();

                interAdapViewAppointments.codeToDeleteSelectedAppointmentRecord(iAppointmentId, holder.getAdapterPosition());
            }
        });

    }



    public static class ViewAppointmentsViewHolder extends RecyclerView.ViewHolder {

        private TextView txtHospitalLocation;
        private TextView txtPreferredDate;
        private TextView txtPreferredTime;
        private TextView txtSpecialist;
        private TextView txtCondition;

        private ImageView imgvDelete;

        public ViewAppointmentsViewHolder(View itemView) {
            super(itemView);

            txtHospitalLocation = (TextView) itemView.findViewById(R.id.txtAppointmentHospital);
            txtPreferredDate = (TextView) itemView.findViewById(R.id.txtAppointmentDate);
            txtPreferredTime = (TextView) itemView.findViewById(R.id.txtAppointmentTime);
            txtSpecialist = (TextView) itemView.findViewById(R.id.txtAppointmentSpecialist);
            txtCondition = (TextView) itemView.findViewById(R.id.txtAppointmentCondition);

            imgvDelete = (ImageView) itemView.findViewById(R.id.imgvAppointmentDelete);

        }
    }
}
