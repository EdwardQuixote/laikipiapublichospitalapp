package u.LPHApp;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import u.LPHApp.Adapters.AdapterAmbulances;
import u.LPHApp.Models.ModelAmbulance;

public class AmbulanceActivity extends AppCompatActivity {

    private ArrayList<ModelAmbulance> arylAmbulances;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ambulance);

        initializeVariablesAndUIObjects();
    }


    /**
     * Method to declare and initialize,
     * class variables and UI Objects.
     *
     * Called in (Override)this.onCreateView();
     */
    private void initializeVariablesAndUIObjects() {

        ModelAmbulance clsAmbulance1 = new ModelAmbulance();
        clsAmbulance1.setsAmbulanceServiceName("St. John's Ambulance");
        clsAmbulance1.setsAmbulanceServiceContactNumber("+254 7070707");

        ModelAmbulance clsAmbulance2 = new ModelAmbulance();
        clsAmbulance2.setsAmbulanceServiceName("Red Cross Ambulance");
        clsAmbulance2.setsAmbulanceServiceContactNumber("+254 7090909");

        ModelAmbulance clsAmbulance3 = new ModelAmbulance();
        clsAmbulance3.setsAmbulanceServiceName("Arrow Web Ambulance Services");
        clsAmbulance3.setsAmbulanceServiceContactNumber("+254 8080808");

        arylAmbulances = new ArrayList<>();
        arylAmbulances.add(clsAmbulance1);
        arylAmbulances.add(clsAmbulance2);
        arylAmbulances.add(clsAmbulance3);

        ListView lstAmbulanceServices = (ListView) this.findViewById(R.id.lstAmbulances);
        if (lstAmbulanceServices != null) {
            lstAmbulanceServices.setAdapter(new AdapterAmbulances(arylAmbulances));
            lstAmbulanceServices.setOnItemClickListener(clkAmbulance);
        }
    }


    /**
     * ListView.OnItemCLickListener interface for Ambulance ListView.
     * Handles clicks on its items.
     *
     * Implemented in this.initializeVariablesAndUIObjects();
     */
    private ListView.OnItemClickListener clkAmbulance = new ListView.OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            String sContactNumber = arylAmbulances.get(position).getsAmbulanceServiceContactNumber();

            Intent inCallAmbulance = new Intent(Intent.ACTION_CALL);
            inCallAmbulance.setData(Uri.parse("tel:" + sContactNumber));
            try {
                startActivity(inCallAmbulance);
            } catch (SecurityException sex) {
                sex.printStackTrace();

                Snackbar.make(findViewById(R.id.lstAmbulances), "App has No permission to Make Call!", Snackbar.LENGTH_LONG).show();
            }
        }
    };
}
