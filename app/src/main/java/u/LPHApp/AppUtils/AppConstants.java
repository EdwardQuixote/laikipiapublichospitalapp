package u.LPHApp.AppUtils;

/**
 * Class that holds Constants used in this App.
 *
 * Created by Edward Ndukui,
 * on Wednesday, 05-Jul-17,
 * at 8:41AM.
 */
public class AppConstants {

    //      General Strings
    public static final String sENCODING_SCHEME = "UTF-8";

    //      Appointment Fields Keys:
    public static final String sPATIENT_NAME = "PATIENT_NAME";
    public static final String sCONTACT_NUMBER = "CONTACT_NUMBER";
    public static final String sHOSPITAL_LOCATION = "HOSPITAL_LOCATION";
    public static final String sPREFERRED_DATE = "PREFERRED_DATE";
    public static final String sPREFERRED_TIME = "PREFERRED_TIME";
    public static final String sSPECIALIST = "SPECIALIST";
    public static final String sCONDITION = "CONDITION";

}
