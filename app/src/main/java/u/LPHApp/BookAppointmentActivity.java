package u.LPHApp;

import android.database.SQLException;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TimePicker;

import java.util.Calendar;

import u.LPHApp.AppUtils.AppConstants;
import u.LPHApp.Database.DBHelper;

public class BookAppointmentActivity extends AppCompatActivity {

    private TextInputLayout tilPatientName;
    private TextInputLayout tilContactNumber;
    private TextInputLayout tilCondition;

    private TextInputEditText tiedPatientName;
    private TextInputEditText tiedContactNumber;
    private TextInputEditText tiedCondition;

    private Spinner spnHospitalLocation;
    private Spinner spnSpecialist;

    private DatePicker dpPreferredDate;
    private TimePicker tpPreferredTime;

    private String sPreferredDate;
    private String sPreferredTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_appointment);

        initializeVariablesAndUIObjects();
    }


    /**
     * Method to declare and initialize,
     * class variables and UI Objects.
     * <p>
     * Called in (Override)this.onCreate();
     */
    private void initializeVariablesAndUIObjects() {

        String[] saryHospitalLocation = {
                "Borana Dispensary",
                "Mpalla Dispensary",
                "Oljogi Dispensary",
                "Nyahururu Private Hospital",
                "Good Hope Hospital",
                "Oljabet Annex and Nursing Home",
                "Maryhill Medical Clinic",
                "Segera Mission Dispensary",
                "Nanyuki Cottage Hospital"
        };
        String[] sarySpecialists = {
                "Dentist",
                "Surgeon",
                "Bone Doctor",
                "Optician",
                "Physician",
                "Post-trauma Specialist",
                "Dermatologist",
                "Neurosurgeon"
        };

        ArrayAdapter<String> adpHospitalLocation = new ArrayAdapter<>(BookAppointmentActivity.this, android.R.layout.simple_dropdown_item_1line, saryHospitalLocation);
        ArrayAdapter<String> adpSpecialists = new ArrayAdapter<>(BookAppointmentActivity.this, android.R.layout.simple_spinner_dropdown_item, sarySpecialists);

        Calendar calCalendar = Calendar.getInstance();
        int iYear = calCalendar.get(Calendar.YEAR);
        int iMonthOfYear = calCalendar.get(Calendar.MONTH);
        int iDayOfMonth = calCalendar.get(Calendar.DAY_OF_MONTH);

        tilPatientName = (TextInputLayout) this.findViewById(R.id.tilBAPatientName);
        tilContactNumber = (TextInputLayout) this.findViewById(R.id.tilBAContactNumber);
        tilCondition = (TextInputLayout) this.findViewById(R.id.tilBACondition);

        tiedPatientName = (TextInputEditText) this.findViewById(R.id.tiedBAPatientName);
        tiedContactNumber = (TextInputEditText) this.findViewById(R.id.tiedBAContactNumber);
        tiedCondition = (TextInputEditText) this.findViewById(R.id.tiedBACondition);

        spnHospitalLocation = (Spinner) this.findViewById(R.id.spnBAHospitalLocation);
        spnSpecialist = (Spinner) this.findViewById(R.id.spnBASpecialist);
        spnHospitalLocation.setAdapter(adpHospitalLocation);
        spnSpecialist.setAdapter(adpSpecialists);

        dpPreferredDate = (DatePicker) this.findViewById(R.id.dpBAPreferredDate);
        tpPreferredTime = (TimePicker) this.findViewById(R.id.tpBAPreferredTime);
        dpPreferredDate.init(iYear, iMonthOfYear, iDayOfMonth, odclPreferredDate);
        tpPreferredTime.setOnTimeChangedListener(otclPreferredTime);

        Button btnBook = (Button) this.findViewById(R.id.btnBABookAppointment);
        btnBook.setOnClickListener(clkBookAppointment);

    }

    /**
     * Method to validate if the data,
     * the user provided is valid.
     * <p>
     * Called in this.clkBookAppointment.onClick();
     *
     * @return (boolean)
     */
    private boolean codeToValidateUserInput() {

        if (tiedPatientName.getText().toString().equalsIgnoreCase("")) {
            tilPatientName.setError("Please Provide the Patient's Name.");

            tiedPatientName.requestFocus();

            return false;
        } else if (tiedContactNumber.getText().toString().equalsIgnoreCase("")) {
            tilContactNumber.setError("Please Provide the Patient's Contact Number.");

            tiedContactNumber.requestFocus();

            return false;
        } else if (tiedCondition.getText().toString().equalsIgnoreCase("")) {
            tilCondition.setError("Please Provide the Patient's Health Condition.");

            tiedCondition.requestFocus();

            return false;
        } else {

            tilPatientName.setError("");
            tilContactNumber.setError("");
            tilCondition.setError("");

            return true;
        }
    }

    /**
     * This method gets final User Input,
     * and Saves it to Database.
     *
     * Called in this.clkBookAppointment.onClick();
     */
    private void codeToSubmitUserDataAndBookAppointment() {

        String sPatientName = tiedPatientName.getText().toString();
        String sContactNumber = tiedContactNumber.getText().toString();
        String sCondition = tiedCondition.getText().toString();

        String sHospitalLocation = spnHospitalLocation.getSelectedItem().toString();
        String sSpecialist = spnSpecialist.getSelectedItem().toString();

        String sDate = this.sPreferredDate;
        String sTime = this.sPreferredTime;

        //  TODO: I want to connect to external database here.












        Bundle bunAppointmentDetails = new Bundle();
        bunAppointmentDetails.putString(AppConstants.sPATIENT_NAME, sPatientName);
        bunAppointmentDetails.putString(AppConstants.sCONTACT_NUMBER, sContactNumber);
        bunAppointmentDetails.putString(AppConstants.sHOSPITAL_LOCATION, sHospitalLocation);
        bunAppointmentDetails.putString(AppConstants.sPREFERRED_DATE, sDate);
        bunAppointmentDetails.putString(AppConstants.sPREFERRED_TIME, sTime);
        bunAppointmentDetails.putString(AppConstants.sSPECIALIST, sSpecialist);
        bunAppointmentDetails.putString(AppConstants.sCONDITION, sCondition);

        try {
            new DBHelper(BookAppointmentActivity.this).codeToWriteToTableAppointments(bunAppointmentDetails);

            tiedPatientName.setText("");
            tiedContactNumber.setText("");
            tiedCondition.setText("");

            Snackbar.make(tiedCondition, "Appointment Booked Successfully.", Snackbar.LENGTH_LONG).show();
        } catch (SQLException sqlex) {
            sqlex.printStackTrace();

            Snackbar.make(tiedCondition, "Appointment not Booked Successfully. Please try again, later.", Snackbar.LENGTH_LONG).show();
        }
    }


    /**
     * View.OnClickListener interface for Views on this Activity.
     * Handles clicks on this Activity.
     * <p>
     * Called in this.initializeVariablesAndUIObjects();
     */
    private View.OnClickListener clkBookAppointment = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

            switch (v.getId()) {

                case R.id.btnBABookAppointment:

                    if (codeToValidateUserInput()) {
                        codeToSubmitUserDataAndBookAppointment();
                    }

                    break;
            }
        }
    };

    /**
     * DatePicker.OnDateChangedListener interface to monitor date changes,
     * on the DatePicker.
     *
     * Implemented in this.initializeVariablesAndUIObjects();
     */
    private DatePicker.OnDateChangedListener odclPreferredDate = new DatePicker.OnDateChangedListener() {

        @Override
        public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

            sPreferredDate = dayOfMonth + "/" + monthOfYear + "/" + year;

        }
    };

    /**
     * TimePicker.OnTimeChangedListener interface to monitor time changes,
     * on the TimePicker.
     *
     * Implemented in this.initializeVariablesAndUIObjects();
     */
    private TimePicker.OnTimeChangedListener otclPreferredTime = new TimePicker.OnTimeChangedListener() {

        @Override
        public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {

            String sTimeOfDay;

            if (hourOfDay > 12) {
                sTimeOfDay = "PM";
            } else if (hourOfDay < 12) {
                sTimeOfDay = "AM";
            } else {
                sTimeOfDay = "AM/PM";
            }

            sPreferredTime = hourOfDay + ":" + minute + " " + sTimeOfDay;

        }
    };
}