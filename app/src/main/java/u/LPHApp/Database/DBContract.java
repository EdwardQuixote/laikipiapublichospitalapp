package u.LPHApp.Database;

import android.provider.BaseColumns;

/**
 * Class to hold Database Constants.
 *
 * Created by Edward Ndukui,
 * on Tuesday, 04-Jul-17,
 * at 5:26PM.
 */
public class DBContract {

    //  DB Name and Version:
    public static final String sDB_NAME = "LAIKIPIA_PUBLIC_HOSPITAL";
    public static final int iDB_VERSION = 1;

    //      Table Names:
    public static final String sTABLE_APPOINTMENTS = "APPOINTMENT";

    //      Create Table Statements:
    public static final String sCREATE_TABLE_APPOINTMENTS = "CREATE TABLE IF NOT EXISTS " + sTABLE_APPOINTMENTS + " ("
            + DBColumns.sColAppointmentID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + DBColumns.sColPatientName + " TEXT,"
            + DBColumns.sColContactNumber + " TEXT,"
            + DBColumns.sColHospitalLocation + " TEXT,"
            + DBColumns.sColPreferredDate + " TEXT,"
            + DBColumns.sColPreferredTime + " TEXT,"
            + DBColumns.sColSpecialist + " TEXT,"
            + DBColumns.sColCondition + " TEXT);";

    //      Select from Tables Statements:
    public static final String sSELECT_ALL_FROM_APPOINTMENTS = "SELECT * FROM " + sTABLE_APPOINTMENTS;

    //      Delete from Tables Statements:
    public static final String sDELETE_FROM_APPOINTMENTS = "DELETE FROM "
            + sTABLE_APPOINTMENTS
            + " WHERE "
            + DBColumns.sColAppointmentID
            + " = ";


    public DBContract() {}


    public static abstract class DBColumns implements BaseColumns {

        public static final String sColAppointmentID = "APPOINTMENT_ID";
        public static final String sColPatientName = "PATIENT_NAME";
        public static final String sColContactNumber = "CONTACT_NUMBER";
        public static final String sColHospitalLocation = "HOSPITAL_LOCATION";
        public static final String sColPreferredDate = "PREFERRED_DATE";
        public static final String sColPreferredTime = "PREFERRED_TIME";
        public static final String sColSpecialist = "SPECIALIST";
        public static final String sColCondition = "CONDITION";

        public static final String[] saryTableAppointmentsColumns = {
                sColAppointmentID,
                sColPatientName,
                sColContactNumber,
                sColHospitalLocation,
                sColPreferredDate,
                sColPreferredTime,
                sColSpecialist,
                sColCondition
        };
    }
}
