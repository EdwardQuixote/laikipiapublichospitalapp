package u.LPHApp.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;

import u.LPHApp.AppUtils.AppConstants;
import u.LPHApp.Models.ModelAppointment;

/**
 * Database Helper class.
 *
 * Created by Edward Ndukui,
 * on Tuesday, 04-Jul-17,
 * at 5:21PM.
 */
public class DBHelper extends SQLiteOpenHelper {

    public DBHelper(Context context) {
        super(context, DBContract.sDB_NAME, null, DBContract.iDB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        this.codeToCreateTablesInDB(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        this.codeToCreateTablesInDB(db);
    }

    @Override
    public SQLiteDatabase getReadableDatabase() {
        return super.getReadableDatabase();
    }

    @Override
    public SQLiteDatabase getWritableDatabase() {
        return super.getWritableDatabase();
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
    }

    @Override
    public void onConfigure(SQLiteDatabase db) {
        super.onConfigure(db);
    }


    /**
     * Method to create Tables in Database.
     *
     * Called in:
     *          this.onCreate();
     *          this.onUpgrade();
     *
     * @param sqLiteDatabase            (SQLiteDatabase)
     */
    private void codeToCreateTablesInDB(SQLiteDatabase sqLiteDatabase) {

        Log.e("DBHelper", "codeToCreateTablesInDB() CALLED.");

        sqLiteDatabase.execSQL(DBContract.sCREATE_TABLE_APPOINTMENTS);
        Log.e("DBHelper", "Table APPOINTMENTS CREATED!");

    }

    /**
     * Method to write Data to Appointments Table.
     *
     * Called in:
     *             - BookAppointmentActivity.codeToSubmitUserDataAndBookAppointment();
     *
     * @param bundleAppointmentDetails      (Bundle)
     */
    public void codeToWriteToTableAppointments(Bundle bundleAppointmentDetails) throws SQLException {

        String sPatientName = bundleAppointmentDetails.getString(AppConstants.sPATIENT_NAME);
        String sContactNumber = bundleAppointmentDetails.getString(AppConstants.sCONTACT_NUMBER);
        String sHospitalLocation = bundleAppointmentDetails.getString(AppConstants.sHOSPITAL_LOCATION);
        String sPreferredDate = bundleAppointmentDetails.getString(AppConstants.sPREFERRED_DATE);
        String sPreferredTime = bundleAppointmentDetails.getString(AppConstants.sPREFERRED_TIME);
        String sSpecialist = bundleAppointmentDetails.getString(AppConstants.sSPECIALIST);
        String sCondition = bundleAppointmentDetails.getString(AppConstants.sCONDITION);

        ContentValues cvAppointmentData = new ContentValues();
        cvAppointmentData.put(DBContract.DBColumns.sColPatientName, sPatientName);
        cvAppointmentData.put(DBContract.DBColumns.sColContactNumber, sContactNumber);
        cvAppointmentData.put(DBContract.DBColumns.sColHospitalLocation, sHospitalLocation);
        cvAppointmentData.put(DBContract.DBColumns.sColPreferredDate, sPreferredDate);
        cvAppointmentData.put(DBContract.DBColumns.sColPreferredTime, sPreferredTime);
        cvAppointmentData.put(DBContract.DBColumns.sColSpecialist, sSpecialist);
        cvAppointmentData.put(DBContract.DBColumns.sColCondition, sCondition);

        Log.e("DBHelper", "codeToWriteToTableAppointments() - Data put into ContentValues SUCCESSFULLY");

        SQLiteDatabase sqldbDatabase = this.getWritableDatabase();
        sqldbDatabase.insert(DBContract.sTABLE_APPOINTMENTS, null, cvAppointmentData);
        Log.e("DBHelper", "codeToWriteToTableAppointments() - Data INSERTED INTO DB SUCCESSFULLY");

        sqldbDatabase.close();
        Log.e("DBHelper", "codeToWriteToTableAppointments() - SQLiteDatabase CLOSED SUCCESSFULLY");

        Log.e("DBHelper", "codeToWriteToTableAppointments() -- DATA WRITTEN SUCCESSFULLY!");
    }

    /**
     * Method to read all Data from Table Appointments.
     *
     * Called in:
     *          - ViewAppointmentsActivity.codeToFetchAppointmentsFromDatabase();
     *
     * @return arylAppointments         (ArrayList<ModelAppointments>);
     * @throws SQLException
     */
    public ArrayList<ModelAppointment> codeToReadFromTableAppointments() throws SQLException {

        Log.e("DBHelper", "codeToReadFromTableAppointments() CALLED!");

        ArrayList<ModelAppointment> arylAppointments = new ArrayList<>();

        SQLiteDatabase sqldbDatabase = this.getReadableDatabase();
        Cursor curReadCursor = sqldbDatabase.rawQuery(DBContract.sSELECT_ALL_FROM_APPOINTMENTS, null);
        if (curReadCursor != null) {

            Log.e("DBHelper", "codeToReadFromTableAppointments() - curReadCursor is NOT NULL!");

            curReadCursor.moveToFirst();

            Log.e("DBHelper", "codeToReadFromTableAppointments() - curReadCursor moveToFirst()");

            while (!(curReadCursor.isAfterLast())) {

                ModelAppointment clsAppointment = new ModelAppointment();
                clsAppointment.setiAppointmentId(curReadCursor.getInt(0));
                clsAppointment.setsPatientName(curReadCursor.getString(1));
                clsAppointment.setsContactNumber(curReadCursor.getString(2));
                clsAppointment.setsHospitalLocation(curReadCursor.getString(3));
                clsAppointment.setsPreferredDate(curReadCursor.getString(4));
                clsAppointment.setsPreferredTime(curReadCursor.getString(5));
                clsAppointment.setsSpecialist(curReadCursor.getString(6));
                clsAppointment.setsCondition(curReadCursor.getString(7));

                arylAppointments.add(clsAppointment);

                Log.e("DBHelper", "codeToReadFromTableAppointments() - curReadCursor READ SUCCESSFUL. arylAppointments: " + arylAppointments.size());

                curReadCursor.moveToNext();

                Log.e("DBHelper", "codeToReadFromTableAppointments() - curReadCursor moveToNext()");
            }

            curReadCursor.close();

            Log.e("DBHelper", "codeToReadFromTableAppointments() - curReadCursor CLOSED");
        }

        return arylAppointments;
    }

    /**
     * Method to delete selected Appointment.
     *
     * Called in:
     *          - ViewAppointmentsActivity.codeToDeleteSelectedAppointmentRecord();
     *
     * @param appointmentIdToDelete             (String[])
     * @throws SQLException
     */
    public void codeToDeleteFromTableAppointments(String[] appointmentIdToDelete) throws SQLException {

        Log.e("DBHelper", "codeToDeleteFromTableAppointments() -- CALLED!");

        SQLiteDatabase sqldbDatabase = this.getWritableDatabase();
        sqldbDatabase.delete(DBContract.sTABLE_APPOINTMENTS, DBContract.DBColumns.sColAppointmentID + " = ?", appointmentIdToDelete);
    }
}
