package u.LPHApp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        initializeVariablesAndUIObjects();
    }


    /**
     * Method to declare and initialize,
     * class variables and UI Objects.
     *
     * Called in (Override)this.onCreate();
     */
    private void initializeVariablesAndUIObjects() {

        Button btnBookAppointment = (Button) this.findViewById(R.id.btnHomeBookAppointment);
        Button btnViewAppointments = (Button) this.findViewById(R.id.btnHomeViewAppointments);
        Button btnCallAmbulance = (Button) this.findViewById(R.id.btnHomeCallAmbulance);
        Button btnLocateHospital = (Button) this.findViewById(R.id.btnHomeLocateHospital);
        btnBookAppointment.setOnClickListener(clkHomeActivity);
        btnViewAppointments.setOnClickListener(clkHomeActivity);
        btnCallAmbulance.setOnClickListener(clkHomeActivity);
        btnLocateHospital.setOnClickListener(clkHomeActivity);

    }


    private View.OnClickListener clkHomeActivity = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

            Intent inHomeActivity = null;
            switch(v.getId()) {

                case R.id.btnHomeBookAppointment:

                    inHomeActivity = new Intent(HomeActivity.this, BookAppointmentActivity.class);
                    break;

                case R.id.btnHomeViewAppointments:
                    inHomeActivity = new Intent(HomeActivity.this, ViewAppointmentsActivity.class);
                    break;

                case R.id.btnHomeCallAmbulance:
                    inHomeActivity = new Intent(HomeActivity.this, AmbulanceActivity.class);
                    break;

                case R.id.btnHomeLocateHospital:
                    inHomeActivity = new Intent(HomeActivity.this, LocateHospitalsActivity.class);
                    break;
            }

            if (inHomeActivity != null) {
                startActivity(inHomeActivity);
            }
        }
    };
}

