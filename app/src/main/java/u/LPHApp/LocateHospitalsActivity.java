package u.LPHApp;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class LocateHospitalsActivity extends FragmentActivity implements OnMapReadyCallback {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_locate_hospitals);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment fragMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapLocateHospitals);
        fragMapFragment.getMapAsync(this);

        Log.e("LocateHospitalActivity", "(Override)onCreate() - getMapAsync() CALLED!");    //  TODO: FOR Testing ONLY

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {

        Log.e("LocateHospitalActivity", "(Override)onMapReady() - CALLED!");    //  TODO: FOR Testing ONLY

        LatLng ltlngOljabetMedical = new LatLng(0.0396759, 36.3515496);
        LatLng ltlngNyahururuPrivate = new LatLng(0.0306928, 36.3612091);
        LatLng ltlngGoodHope = new LatLng(0.0354645, 36.3639937);
        LatLng ltlnMaryhillMedical = new LatLng(0.0384897, 36.3611331);
        LatLng ltlnSegeraMission = new LatLng(0.1516037, 36.885277);
        LatLng ltlnNanyukiCottage = new LatLng(0.0027234, 37.0763373);

        googleMap.setBuildingsEnabled(true);
        try {
            googleMap.setMyLocationEnabled(true);
        } catch (SecurityException sex) {
            sex.printStackTrace();
        }

        googleMap.addMarker(new MarkerOptions().position(ltlngOljabetMedical).title("Oljabet Annex and Nursing Home"));
        googleMap.addMarker(new MarkerOptions().position(ltlngNyahururuPrivate).title("Nyahururu Private Hospital"));
        googleMap.addMarker(new MarkerOptions().position(ltlngGoodHope).title("Good Hope Hospital"));
        googleMap.addMarker(new MarkerOptions().position(ltlnMaryhillMedical).title("Maryhill Medical Clinic"));
        googleMap.addMarker(new MarkerOptions().position(ltlnSegeraMission).title("Segera Mission Dispensary"));
        googleMap.addMarker(new MarkerOptions().position(ltlnNanyukiCottage).title("Nanyuki Cottage Hospital"));
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(ltlngNyahururuPrivate, 13));

    }
}
