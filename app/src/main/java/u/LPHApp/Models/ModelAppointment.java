package u.LPHApp.Models;

/**
 * Data Model class for Appointments.
 *
 * Created by Edward Ndukui,
 * on Wednesday, 05-Jul-17,
 * at 9:03AM.
 */
public class ModelAppointment {

    private String sPatientName;
    private String sContactNumber;
    private String sHospitalLocation;
    private String sPreferredDate;
    private String sPreferredTime;
    private String sSpecialist;
    private String sCondition;

    private int iAppointmentId;

    public int getiAppointmentId() {
        return iAppointmentId;
    }

    public void setiAppointmentId(int iAppointmentId) {
        this.iAppointmentId = iAppointmentId;
    }

    public String getsPatientName() {
        return sPatientName;
    }

    public void setsPatientName(String sPatientName) {
        this.sPatientName = sPatientName;
    }

    public String getsContactNumber() {
        return sContactNumber;
    }

    public void setsContactNumber(String sContactNumber) {
        this.sContactNumber = sContactNumber;
    }

    public String getsHospitalLocation() {
        return sHospitalLocation;
    }

    public void setsHospitalLocation(String sHospitalLocation) {
        this.sHospitalLocation = sHospitalLocation;
    }

    public String getsPreferredDate() {
        return sPreferredDate;
    }

    public void setsPreferredDate(String sPreferredDate) {
        this.sPreferredDate = sPreferredDate;
    }

    public String getsPreferredTime() {
        return sPreferredTime;
    }

    public void setsPreferredTime(String sPreferredTime) {
        this.sPreferredTime = sPreferredTime;
    }

    public String getsSpecialist() {
        return sSpecialist;
    }

    public void setsSpecialist(String sSpecialist) {
        this.sSpecialist = sSpecialist;
    }

    public String getsCondition() {
        return sCondition;
    }

    public void setsCondition(String sCondition) {
        this.sCondition = sCondition;
    }
}
