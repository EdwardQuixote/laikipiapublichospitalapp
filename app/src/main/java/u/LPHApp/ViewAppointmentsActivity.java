package u.LPHApp;

import android.database.SQLException;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.util.ArrayList;

import u.LPHApp.Adapters.AdapterRVViewAppointments;
import u.LPHApp.Database.DBHelper;
import u.LPHApp.Models.ModelAppointment;

public class ViewAppointmentsActivity extends AppCompatActivity implements AdapterRVViewAppointments.InterfaceAdapViewAppointments {

    private AdapterRVViewAppointments clsAdapterAppointments;

    private RecyclerView rvAppointments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_appointments);

        initializeVariablesAndUIObjects();
    }


    /**
     * Method to declare and initialize
     * class variables,
     * and UI Objects.
     *
     * Called in (Override)this.onCreate();
     */
    private void initializeVariablesAndUIObjects() {

        clsAdapterAppointments = new AdapterRVViewAppointments(ViewAppointmentsActivity.this, codeToFetchAppointmentsFromDatabase());

        LinearLayoutManager llmLayoutManager = new LinearLayoutManager(ViewAppointmentsActivity.this);

        rvAppointments = (RecyclerView) this.findViewById(R.id.rvViewAppointments);
        rvAppointments.setLayoutManager(llmLayoutManager);
        rvAppointments.setItemAnimator(new DefaultItemAnimator());
        rvAppointments.setHasFixedSize(false);
        rvAppointments.setAdapter(clsAdapterAppointments);

    }

    /**
     * This method fetches Appointment records.
     * It calls a method that queries the database.
     *
     * Called in this.initializeVariablesAndUIObjects();
     *
     * @return arylAppointments         (ArrayList<ModelAppointment>);
     */
    private ArrayList<ModelAppointment> codeToFetchAppointmentsFromDatabase() {

        Log.e("ViewAppointments", "codeToFetchAppointmentsFromDatabase() CALLED");  //  TODO: For Testing ONLY.

        ArrayList<ModelAppointment> arylAppointments = new ArrayList<>();

        try {

            arylAppointments = new DBHelper(ViewAppointmentsActivity.this).codeToReadFromTableAppointments();

            Log.e("ViewAppointments", "arylAppointments: " + arylAppointments.size());  //  TODO: For Testing ONLY.
        } catch (SQLException sqlex) {
            sqlex.printStackTrace();
        }

        return arylAppointments;
    }


    @Override
    public void codeToDeleteSelectedAppointmentRecord(int appointmentId, int adapterPosition) {

        Log.e("ViewAppointments", "codeToDeleteSelectedAppointemntRecord() -- appointmentId: " + appointmentId);

        try {
            String[] saryAppointmentToDelete = {Integer.toString(appointmentId)};
            new DBHelper(ViewAppointmentsActivity.this).codeToDeleteFromTableAppointments(saryAppointmentToDelete);

            Log.e("ViewAppointments", "codeToDeleteSelectedAppointmentRecord() -- Data Deleted Successfully!");

            clsAdapterAppointments = new AdapterRVViewAppointments(ViewAppointmentsActivity.this, codeToFetchAppointmentsFromDatabase());
            rvAppointments.setAdapter(clsAdapterAppointments);
        } catch (SQLException sex) {
            Log.e("ViewAppointments", "codeToDeleteSelectedAppointmentRecord() -- Data NOT Deleted Successfully!");

            sex.printStackTrace();
        }
    }
}
